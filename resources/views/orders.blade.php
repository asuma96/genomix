@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="top_chx">
            <a href="/subscribers">
                <button class="btn btn-sm btn-primary">Back</button>
            </a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>OrderID</th>
                <th>Shipping Carrier</th>
                <th>StatusDate</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if (count($orders))
                @foreach ($orders as $item)
                    <tr>
                        <th>{{$item->OrderID}}</th>
                        <th>{{$item->ShippingCarrier}}</th>
                        <th>{{$item->StatusDate}}</th>
                        <th>{{$item->Status}}</th>
                        <th>
                            <div class="text-center">
                                <a href="/order/{{$item->id}}" class="text-center">
                                    <button class="btn btn-sm btn-primary text-center" onclick="disabled = true">
                                        Refresh
                                    </button>
                                </a>
                            </div>
                        </th>
                    </tr>
                @endforeach
            @else
                <tr>
                    <th colspan="4" class="text-center">There are no orders yet</th>
                </tr>
            @endif
        </table>
        </tbody>
        <div class="text-center">{{$orders->links()}}</div>
    </div>
@endsection