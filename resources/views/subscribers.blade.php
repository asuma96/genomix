@extends('layouts.app')
@section('content')
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Shipping address</th>
                <th>Orders count</th>
                <th>Subscribed</th>
                <th>Check orders</th>
            </tr>
            </thead>
            @if (count($subscribers))
                @foreach ($subscribers as $subscriber)
                    <tr>
                        <th>{{$subscriber->first_name ?: 'N/A'}}</th>
                        <th>{{$subscriber->last_name ?: 'N/A'}}</th>
                        <th>{{$subscriber->email ?: 'N/A'}}</th>
                        <th>{{$subscriber->phone ?: 'N/A'}}</th>
                        <th>{{$subscriber->shipping_address ?: 'N/A'}}</th>
                        <th>{{$subscriber->orders_count}}</th>
                        <th>{{(new DateTime($subscriber->created_at))->format('Y.m.d')}}</th>
                        <th>
                            <div class="text-center">
                                <a href="/orders/{{$subscriber->id}}" class="text-center">
                                    <button class="btn btn-sm btn-primary text-center">Orders</button>
                                </a>
                            </div>
                        </th>
                    </tr>
                @endforeach
            @else
                <th colspan="8" class="text-center">There are no subscribers yet</th>

            @endif
        </table>
        <div class="text-center">{{$subscribers->links()}}</div>
    </div>
@endsection