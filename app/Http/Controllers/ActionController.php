<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Stripe\Stripe;
use Stripe\Invoice;

class ActionController extends Controller
{
    private $xymogen;
    private $pages;

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $this->xymogen = new XymogenController();
        $this->pages = new PagesController();
    }

    private function checkPaysCount(Subscriber $subscriber)
    {
        $after = [];
        $hasMore = true;
        $count = 0;
        while ($hasMore) {
            $pays = Invoice::all(array_merge(['limit' => '100', 'subscription' => $subscriber->subscription_id], $after));
            $after = ['starting_after' => end($pays->data)->id];
            $hasMore = $pays->has_more;
            foreach ($pays->data as $pay) {
                if ($pay->__get('paid')) {
                    $count++;
                }
            }
        }

        return $count;
    }

    public function dispatchProducts()
    {
        foreach (Subscriber::all() as $subscriber) {
            if ($subscriber->orders()->get()){
            foreach ($subscriber->orders()->get() as $sub) {
                $this->pages->reloadOrders($sub->id);
            }
            }
            if ($this->checkPaysCount($subscriber) > $subscriber->shipped_count) {
                $data = [
                    'CustOrderID' => $subscriber->subscription_id,
                    'Items' => [[$subscriber->products_array[$subscriber->shipped_count], '1']],
                    'FName' => $subscriber->first_name ?: 'N/A',
                    'LName' => $subscriber->last_name ?: 'N/A',
                    'Address1' => $subscriber->shipping_address ?: 'N/A',
                    'Address2' => $subscriber->shipping_country ?: 'N/A',
                    'City' => $subscriber->shipping_city ?: 'N/A',
                    'State' => $subscriber->shipping_state ?: 'N/A',
                    'Zip' => $subscriber->shipping_zip ?: 'N/A'
                ];
                $orderId = json_decode($this->xymogen->submitOrder($data))->OrderID;
                $orderData = json_decode($this->xymogen->getOrderStatusList([$orderId]))->Orders[0];
                $subscriber->orders()->create([
                    'OrderID' => $orderData->OrderID,
                    'ShippingCarrier' => $orderData->ShippingCarrier,
                    'Status' => $orderData->Status,
                    'StatusDate' => $orderData->StatusDate
                ]);
                $subscriber->increment('shipped_count');
            }
        }

        return 'ok';
    }
}
