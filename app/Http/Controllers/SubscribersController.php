<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use App\Http\Requests\FunnelHookRequest;

class SubscribersController extends Controller
{
    public function newSubscriber(FunnelHookRequest $request)
    {
        $data = $request->purchase;
        if (Subscriber::where('subscription_id', $data['subscription_id'])->first()) {
            return response(['msg' => 'already exists']);
        }
        //foreach ($data['products'] as $p) {
        //    $products[] = $p['id'];
        //}
        $contact = $data['contact'];
        if ($data['subscription_id'] == Null) {
            return response('false subscription_id');
        }
        Subscriber::create([
            'email' => $contact['email'],
            'products' => json_encode(['MELA', '5MTHF', '5MTHFES', '8PN', 'ACTIVN', 'ADMAN', 'ADRENAL', 'ADRMAX', 'AEB', 'AEC', 'AESGS', 'AEW']),
            'first_name' => $contact['first_name'],
            'last_name' => $contact['last_name'],
            'phone' => $contact['phone'],
            'shipping_address' => $contact['address'],
            'shipping_city' => $contact['city'],
            'shipping_country' => $contact['country'],
            'shipping_state' => $contact['state'],
            'shipping_zip' => $contact['zip'],
            'subscription_id' => $data['subscription_id']
        ]);

        return response();
    }
}
