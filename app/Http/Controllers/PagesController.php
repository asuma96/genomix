<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Subscriber;

class PagesController extends Controller
{
    private $xymogen;

    public function __construct()
    {
        $this->xymogen = new XymogenController();
    }

    public function subscribers()
    {
        return view('subscribers', ['subscribers' => Subscriber::orderBy('created_at', 'desc')->paginate()]);
    }

    public function orders(Subscriber $subscriber)
    {
        return view('orders', ['orders' => $subscriber->orders()->paginate()]);
    }

    public function reloadOrders($id)
    {
        $OrderID = Order::query()->whereId($id)->first();
        $orderData = json_decode($this->xymogen->getOrderStatusList([$OrderID->OrderID]))->Orders[0]->Status;
        $OrderID->Status = $orderData;
        $OrderID->save();
        return back();
    }
}
