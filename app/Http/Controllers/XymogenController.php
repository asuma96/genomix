<?php

namespace App\Http\Controllers;

class XymogenController extends Controller
{
    public function submitOrder(array $data)
    {
        return $this->makeRequest('submitOrder', $data);
    }

    public function productList($data = '')
    {
        return $this->makeRequest('productList', $data);
    }

    public function productInventory()
    {
        return $this->makeRequest('productInventory');
    }

    public function getOrderStatusList(array $ids)
    {
        //test ID = 2583
        return $this->makeRequest('getOrderStatusList', ['ids' => $ids]);
    }

    private function makeRequest($method, $data = [])
    {
        $auth = ['CustomerID' => env('XYMOGEN_USER'), 'Pass' => env('XYMOGEN_PASS')];
        $ch = curl_init(env('XYMOGEN_URL') . $method);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array_merge($auth, $data ?: [])));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
