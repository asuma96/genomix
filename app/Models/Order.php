<?php

namespace App\Models;

use App\Models\Subscriber;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'subscriber_id',
        'OrderID',
        'ShippingCarrier',
        'Status',
        'StatusDate'
    ];
    public function subscribers()
    {
        return $this->belongsTo(Subscriber::class);
    }
}
