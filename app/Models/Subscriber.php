<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscriber extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'email',
        'products',
        'first_name',
        'last_name',
        'phone',
        'shipping_address',
        'shipping_city',
        'shipping_country',
        'shipping_state',
        'shipping_zip',
        'shipped_count',
        'subscription_id'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends = ['orders_count'];
    //protected $products = ['products_array'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function getOrdersCountAttribute()
    {
        return $this->orders()->count();
    }
    public function getProductsArrayAttribute(){
        return json_decode($this->products);
    }
}
