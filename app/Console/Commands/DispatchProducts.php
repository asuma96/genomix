<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ActionController;


class DispatchProducts extends Command
{
    private $action;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatch_products:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking ordered products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->action = new ActionController();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->action->dispatchProducts();
    }
}
