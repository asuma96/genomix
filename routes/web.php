<?php
Route::get('/', function () {
    return redirect()->to('login');
});
Auth::routes();


//for cron
Route::get('/dispatchProducts', 'ActionController@dispatchProducts');
Route::get('/checkPaysCount', 'ActionController@checkPaysCount');

// for the Clickfunnel webhook
Route::post('/receive-hook', 'SubscribersController@newSubscriber');
Route::post('/', 'SubscribersController@newSubscriber');
Route::post('/receive-hook/funnel_webhooks/test', function () {
    return response();
});
Route::group(['middleware' => 'auth'], function () {
    Route::get('/subscribers', 'PagesController@subscribers');
    Route::get('/orders/{subscriber}', 'PagesController@orders');
    Route::get('/order/{id}', 'PagesController@reloadOrders');
});

Route::any('{all}', function(){
    return view('errors.404');
})->where('all', '.*');